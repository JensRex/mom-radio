# mom-radio
RPI Internet radio for my mom

Uses a Raspberry Pi, with a [Display-O-Tron](https://github.com/pimoroni/displayotron) HAT for buttons and LCD status display.

This project is designed to be parent-proof. It has two functions. A button to start playing Radio Berlin. Another button to stop playing Radio Berlin. **THAT'S IT.** There's no AirPlay, Spotify, Bluetooth, garage door opener, blinkenlights, media library, thermostat, app store, voice assistant or whatever. There are no builtin tinny speakers, because my mom has a proper amp and floor standing speakers, like a responsible adult. And there aren't 50000 preloaded radio stations. There's one.

Every piece of streaming radio hardware I've found was either far too complex, or had too many unneeded or useless functions that I don't need. So I decided to build my own of course.

My mom isn't a moron, but she's not interested in technology.

NOTE: If running this as a systemd service, you need to run PulseAudio in system mode.

TODO:
* ~~Graceful shutdown of the Pi~~
* ~~Dim the display after N minutes of inactivity?~~
* ~~Probably use something else than VLC, because VLC is terrible and also complete overkill~~
  * Switched to `mpg123`.
* Somehow extract stream metadata and display it on the LCD
  * VLC outputs Icy-Metadata only when verbose debugging is turned on, and I don't know how to get at that data other than very dirty `grep` hacks.
  * ~~`mpg123` doesn't parse Icy-Metadata when receiving data through a pipe, for some reason, and I have to pipe from `curl` because `mpg123` doesn't support HTTPS.~~
    * This appears to have been fixed upstream.
    * Crazy idea: Write my own stream parser.
* Maybe handle power loss in a better way (ie. at all).
  * Ideal solution would be a read-only filesystem.
    * Advantages:
      * Pretty much bullet proof.
      * Interesting problem to solve.
    * Disadvantages:
      * Updating the system will be a pain. It might not be so critical in this case, but I don't like having anything interact with the Internet while not being updated.
  * More hacky solution would involve some capacitor or LiIon cell circuitry.
    * Advantages:
      * Manipulating the file system isn't tedious.
    * Disadvantages:
      * Needs hardware, costs money.
      * GPIO pins are already occupied by LCD+buttons module.
      * Need to write more code.
      * Makes an ugly device even uglier.
