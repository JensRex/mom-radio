#!/usr/bin/env python3

# Simple Radio Streaming interface for Raspberry Pi using LCD+buttons HAT
# by Jens Rex <jens@jensrex.dk>
# Released under the Unlicense (https://unlicense.org/)

import dothat.lcd as lcd
import dothat.backlight as backlight
import dothat.touch as nav
import subprocess
import signal
import atexit
import os
import math
import socket
import time
from threading import Timer
vlc = None  # No longer using VLC. In retrospect, this was a poor choice of variable name
timer_dim = None


def backlight_dim():
    global timer_dim
    if hasattr(timer_dim, "interval"):  # abort timer if running, to avoid duplicate timers
        timer_dim.cancel()
    timer_dim = Timer(10, backlight_dim_timer)
    timer_dim.start()


def backlight_dim_timer():
    backlight.leds = [math.floor(x/2) for x in backlight.leds]  # read all rgb values and divide by 2
    backlight.update()


@nav.on(nav.LEFT)  # Play Radio Berlin
def handle_left(ch, evt):
    lcd.clear()
    lcd.write("Afspiller:")
    lcd.set_cursor_position(0, 1)
    lcd.write("Radio Berlin")
    backlight.rgb(0, 255, 0)  # Green when playing
    backlight_dim()
    global vlc
    if not hasattr(vlc, "pid"):  # Is VLC running?
        vlc = subprocess.Popen(["curl -Ls https://stream.rtlradio.de/rtl-de-beste-hits/mp3-192 | mpg123-pulse -q --buffer 1024 --preload --no-gapless -"], shell=True, preexec_fn=os.setsid)
        print("Starting playback")
    else:  # Yes
        print("Player already running as PID:", str(vlc.pid))


@nav.on(nav.RIGHT)  # Do not play Radio Berlin
def handle_right(ch, evt):
    lcd.clear()
    lcd.write("Afspiller:")
    lcd.set_cursor_position(0, 1)
    lcd.write("Stoppet")
    backlight.rgb(255, 0, 0)  # Red when stopped
    backlight_dim()
    global vlc
    if hasattr(vlc, "pid"):  # Is player running?
        os.killpg(os.getpgid(vlc.pid), signal.SIGTERM)
        print("Stopping playback")
        vlc = None
    else:  # Nope
        print("Playback already stopped")


@nav.on(nav.CANCEL)  # Shut down the device
def handle_cancel(ch, evt):
    print("Powering off device")
    tidyup()
    os.system('sudo systemctl poweroff')


def tidyup():  # Reset display on exit
    backlight.off()
    lcd.clear()


# Reset display on startup, and set it to indicate a stopped state
backlight.rgb(255, 250, 250)
lcd.write("Vent...")  # en="Wait..."
time.sleep(12)  # Let the system finish booting.
lcd.clear()
lcd.write("Afspiller:")  # en="Playback:"
lcd.set_cursor_position(0, 1)
lcd.write("Stoppet")
backlight.rgb(255, 0, 0)
backlight_dim()

atexit.register(tidyup)

signal.pause()
